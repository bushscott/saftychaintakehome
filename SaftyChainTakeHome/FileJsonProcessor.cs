﻿using System;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SaftyChainTakeHome
{
    public class FileJsonProcessor : IFileJsonProcessor
    {
        public async Task<List<DataModel>> ProcessAsync(string inputDirectory)
        {
            List<DataModel> resultRecords = new List<DataModel>();
            if (Directory.Exists(inputDirectory))
            {
                foreach (string? filePath in Directory.GetFiles(inputDirectory, "*.txt"))
                {
                    try
                    {
                        using StreamReader reader = File.OpenText(filePath);
                        JToken fileJToken = await JToken.ReadFromAsync(new JsonTextReader(reader));
                        switch (fileJToken.Type)
                        {
                            case JTokenType.Array:
                                if (fileJToken.ToObject<DataModel[]>()?.Length > 0) { resultRecords.AddRange(fileJToken.ToObject<DataModel[]>()); }
                                else { Console.WriteLine($"File {filePath} skipped as it did not match model schema."); }
                                break;
                            case JTokenType.Object:
                                if (fileJToken.ToObject<DataModel>() != null) { resultRecords.Add(fileJToken.ToObject<DataModel>()); }
                                else { Console.WriteLine($"File {filePath} skipped as it did not match model schema."); }
                                break;
                            default:
                                throw new JsonSerializationException($"File {filePath} skipped as its structure does not match accepted Array or Object Json.");
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message + $"File {filePath} Skipped.");
                    }
                }
            }
            else
            {
                throw new DirectoryNotFoundException($"{inputDirectory} is not a valid path.");
            }
            return resultRecords;
        }
    }
}