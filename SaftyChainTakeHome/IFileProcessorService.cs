﻿using System;
namespace SaftyChainTakeHome
{
    public interface IFileProcessorService
    {
        public Task ExecuteAsync(string? directory);
    }
}