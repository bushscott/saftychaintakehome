﻿using System;
using System.Reflection;
using System.Text;

namespace SaftyChainTakeHome
{
    public class FileProcessorService : IFileProcessorService
    {
        private readonly IFileJsonProcessor _fileJsonProcessor;

        public FileProcessorService(IFileJsonProcessor fileJsonProcessor) =>
            _fileJsonProcessor = fileJsonProcessor;

        public async Task ExecuteAsync(string? directory)
        {
            await WriteRecordsToFile(await _fileJsonProcessor.ProcessAsync(directory), directory);
        }

        private static async Task WriteRecordsToFile(List<DataModel> records, string inputDirectory)
        {
            if (records.Count > 0)
            {
                bool isFirstIteration = true;
                StringBuilder sb = new StringBuilder();
                PropertyInfo[] Props = typeof(DataModel).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (DataModel item in records)
                {
                    string[] propertyNames = item.GetType().GetProperties().Select(p => p.Name).ToArray();
                    foreach (var prop in propertyNames)
                    {
                        if (isFirstIteration == true)
                        {
                            for (int j = 0; j < propertyNames.Length; j++)
                            {
                                sb.Append(propertyNames[j] + ',');
                            }
                            sb.Remove(sb.Length - 1, 1);
                            sb.Append("\r\n");
                            isFirstIteration = false;
                        }
                        object propValue = item.GetType().GetProperty(prop).GetValue(item, null);
                        sb.Append(propValue + ",");
                    }
                    sb.Remove(sb.Length - 1, 1);
                    sb.Append("\r\n");
                }
                await File.WriteAllTextAsync(Path.Combine(inputDirectory, "results.csv"), sb.ToString());
            }
            else
            {
                Console.WriteLine("No Records Extracted.");
            }
        }
    }
}