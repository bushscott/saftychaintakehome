﻿using System;
using Newtonsoft.Json;

namespace SaftyChainTakeHome
{
    public class DataModel
    {
        [JsonProperty("Id")]
        public string? Id { get; set; }
        [JsonProperty("Name")]
        public string? Name { get; set; }
        [JsonProperty("CreatedOn")]
        public string? CreatedOn { get; set; }
        [JsonProperty("CreatedBy")]
        public string? CreatedBy { get; set; }
    }
}