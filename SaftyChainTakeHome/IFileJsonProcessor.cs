﻿using System;
namespace SaftyChainTakeHome
{
    public interface IFileJsonProcessor
    {
        public Task<List<DataModel>> ProcessAsync(string directory);
    }
}