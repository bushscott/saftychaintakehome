﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SaftyChainTakeHome;

using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((_, services) =>
        services.AddTransient<IFileJsonProcessor, FileJsonProcessor>()
                .AddTransient<IFileProcessorService ,FileProcessorService>())
    .Build();

await CombineFiles(host.Services);

static async Task CombineFiles(IServiceProvider services)
{
    try
    {
        using IServiceScope serviceScope = services.CreateScope();
        IServiceProvider provider = serviceScope.ServiceProvider;

        IFileProcessorService fileProcessorService = provider.GetRequiredService<IFileProcessorService>();

        Console.Write("Please enter a directory: ");
        string? directory = Console.ReadLine();
        await fileProcessorService.ExecuteAsync(directory);
    }
    catch (Exception e)
    {
        Console.WriteLine(e.Message);
        Console.ReadLine();
    }
}